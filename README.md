# DOSE

DOSE is an R package for Disease Ontology Semantic and Enrichment analysis.It implemented five methods proposed by Resnik, Schlicker, Jiang, Lin and Wang respectively for measuring DO semantic similarities, and hypergeometric test for enrichment analysis.


To install or update, run:

	source("http://bioconductor.org/biocLite.R")
	biocLite("DOSE")
	

# Development

To install the development version of DOSE, run:

	install.packages("DOSE", repos="http://www.bioconductor.org/packages/devel/bioc", type="source")
	

